module twstock

go 1.13

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/PuerkitoBio/goquery v1.5.0
	github.com/axgle/mahonia v0.0.0-20180208002826-3358181d7394
	github.com/carlescere/scheduler v0.0.0-20170109141437-ee74d2f83d82
	github.com/fatih/camelcase v1.0.0 // indirect
	github.com/fatih/structs v1.1.0 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/koding/multiconfig v0.0.0-20171124222453-69c27309b2d7
	github.com/stretchr/testify v1.4.0 // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
