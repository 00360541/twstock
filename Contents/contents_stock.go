package Contents

type StockConf struct{
	Twsecode string `envField:"ServeConf:Twsecode"` // TWSE equities = 上市證券名錄 URL
	Tpexcode string `envField:"ServeConf:Tpexcode"` // TPEx equities = 上櫃證券名錄 URL
}