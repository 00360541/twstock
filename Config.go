package twstock

import (
	"errors"
	"fmt"
	_ "github.com/joho/godotenv/autoload"
	"github.com/koding/multiconfig"
	"sync"
	"twstock/Contents"
	"twstock/Options"
)

//======================== Interface ==============================
/*
	v0.4 實作從 env 讀取 config
	v0.4 有用到在讀，沒用到就算了
*/
// 實際上會有什麼不知道，先寫一個出來讓這個類別可以使用
type IConfig interface {}

// 必備的先寫在裡面，沒有的當選寫讓 custom 有機會用
type Config struct {
	StockConf *Contents.StockConf
	Custom    map[string]interface{}
}

var _ IConfig = &Config{}
//======================== Instance ==============================

// 取得 Env 內容
func (c *Config)GetEnv(prefix string) (interface{}, error){
	if val,ok:= c.Custom[prefix];ok{
		return val, nil
	}
	fmt.Println(fmt.Errorf("[ConfigSystem] Config Not Found in Prefix `%s`, Please Check", prefix))
	return nil, errors.New("config not found")
}

// 讀入 .env 內容
func (c *Config)loadEnv(envPrefix string, conf interface{}, opts *Options.LoadEnvOptions){
	InstantiateLoader := &multiconfig.EnvironmentLoader{
		Prefix:    envPrefix,
		CamelCase: *opts.CamelCase,
	}
	err := InstantiateLoader.Load(conf)
	if err != nil {
		panic(err)
	}
}

func (c *Config) SystemExternalEnv(envPrefix string, conf interface{}, opts ...*Options.LoadEnvOptions) {
	envOpt := Options.MergeLoadEnvOptions(opts...)
	c.loadEnv(envPrefix, conf, envOpt)
}

func (c *Config) LoadExternalEnv(envPrefix string, conf interface{}, opts ...*Options.LoadEnvOptions) {
	envOpt := Options.MergeLoadEnvOptions(opts...)
	c.loadEnv(envPrefix, conf, envOpt)
	c.Custom[envPrefix] = conf
}

// 全域唯一取得config

var Cfg Config
var once sync.Once

func GetConfig() Config {
	once.Do(func() {
		cfg := &Config{
			StockConf: &Contents.StockConf{},
			Custom: make(map[string]interface{}),
		}
		cfg.SystemExternalEnv("SERVE", cfg.StockConf)
		Cfg = *cfg
	})
	return Cfg
}
