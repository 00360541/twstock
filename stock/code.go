package stock

import (
	"github.com/PuerkitoBio/goquery"
	"strings"
	"time"
	"twstock"
	"twstock/Utils"
)
type EquitiesCodes struct{
	EquitiesCode []equitiesCode
}

// 股票代碼
type equitiesCode struct {
	TYPE   string    `bson:"TYPE"`
	Code   string    `bson:"Code"`
	Name   string    `bson:"Name"`
	ISIN   string    `bson:"ISIN"`
	Start  time.Time `bson:"Start"`
	Market string    `bson:"Market"`
	Group  string    `bson:"Group"`
	CFI    string    `bson:"CFI"`
}

// 取得上市或上櫃的資訊
func (e *EquitiesCodes)GetCode(TYPE int) {
	url:=make([]string,0)
	switch TYPE {
		case 0:
			url = append(url,twstock.GetConfig().StockConf.Twsecode)
		case 1:
			url = append(url,twstock.GetConfig().StockConf.Tpexcode)
		case 2:
			url = append(url,twstock.GetConfig().StockConf.Twsecode)
			url = append(url,twstock.GetConfig().StockConf.Tpexcode)
	}
	e.EquitiesCode = fetchData(url)
}



// 取得資訊
func fetchData(url []string) []equitiesCode {
	header:= make(map[string]string)
	equities := make([]equitiesCode, 0)
	for _,item :=range url{
		doc, err := Utils.GetDomElement(item, header)
		Utils.ProcessErr(err)
		doc.Find("table").Each(func(i int, dom *goquery.Selection) {
			var TYPE string
			dom.Find("tr").Each(func(itr int, dtr *goquery.Selection) {
				if itr >0{
					//初始化一個新的 equities
					equitie := &equitiesCode{}
					// 定義有價債券的種類
					if dtr.Find("td").Eq(1).Text() == "" {
						TYPE = Utils.ConvertToString(dtr.Find("td").Eq(0).Text(), "Big5", "utf-8")
					} else {
						CodeAndName := strings.Split(Utils.ConvertToString(dtr.Find("td").Eq(0).Text(), "Big5", "utf-8"), "　")
						// 取得代碼
						equitie.Code  = CodeAndName[0]
						// 取得名字
						equitie.Name  = CodeAndName[1]
						// 指派 ISIN
						equitie.ISIN = Utils.ConvertToString(dtr.Find("td").Eq(1).Text(), "Big5", "utf-8")
						// 上市日
						equitie.Start = stringToTime(Utils.ConvertToString(dtr.Find("td").Eq(2).Text(), "Big5", "utf-8"))
						// 市場別
						equitie.Market = Utils.ConvertToString(dtr.Find("td").Eq(3).Text(), "Big5", "utf-8")
						//產業別
						equitie.Group = Utils.ConvertToString(dtr.Find("td").Eq(4).Text(), "Big5", "utf-8")
						// CFICode
						equitie.CFI = Utils.ConvertToString(dtr.Find("td").Eq(5).Text(), "Big5", "utf-8")
					}
					equitie.TYPE = TYPE
					equities = append(equities, *equitie)
				}
			})
		})
	}
	return equities
}

// 轉換時間
func stringToTime(item string) time.Time {
	data := strings.Split(item, "/")
	format := data[0] + "-" + data[1] + "-" + data[2] + "T00:00:00-0800"
	layout := "2006-01-02T15:04:05-0700"
	t2, _ := time.Parse(layout, format)
	return t2
}