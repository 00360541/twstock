package twstock

import (
	"fmt"
	"github.com/carlescere/scheduler"
)
func main(){
	job := func() {
		fmt.Println("Time's up!")
	}
	_, _ = scheduler.Every(5).Seconds().Run(job)
	select{}

}
