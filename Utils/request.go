package Utils

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"net/http"
)

func GetDomElement(url string, header map[string]string) (*goquery.Document, error) {
	client := &http.Client{}
	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		panic(err)
	}
	if request == nil {
		panic("Error")
	}
	for k, v := range header {
		request.Header.Set(k, v)
	}
	resp, err := client.Do(request)
	if resp == nil {
		fmt.Println("Client Error")
		return nil, nil
	}
	doc, err := goquery.NewDocumentFromReader(resp.Body)
	defer resp.Body.Close()
	return doc, err

}
